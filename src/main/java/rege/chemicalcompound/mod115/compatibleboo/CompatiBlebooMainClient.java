package rege.chemicalcompound.mod115.compatibleboo;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.Environment;
import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.block.entity.SignBlockEntityRenderer;

import static rege.chemicalcompound.mod115.compatibleboo.CompatiBlebooMain.BAMBOO_DOOR;
import static rege.chemicalcompound.mod115.compatibleboo.CompatiBlebooMain.BAMBOO_TRAPDOOR;

@Environment(EnvType.CLIENT)
public class CompatiBlebooMainClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        // This entrypoint is suitable for setting up client-specific logic, such as rendering.
        BlockRenderLayerMap.INSTANCE.putBlock(BAMBOO_DOOR, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(BAMBOO_TRAPDOOR, RenderLayer.getCutout());
        BlockEntityRendererRegistry.INSTANCE.register(BlockEntityType.SIGN, SignBlockEntityRenderer::new);
    }
}
