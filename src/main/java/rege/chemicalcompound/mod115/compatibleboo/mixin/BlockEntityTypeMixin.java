package rege.chemicalcompound.mod115.compatibleboo.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

import net.minecraft.block.Block;

import static rege.chemicalcompound.mod115.compatibleboo.CompatiBlebooMain.BAMBOO_SIGN;
import static rege.chemicalcompound.mod115.compatibleboo.CompatiBlebooMain.BAMBOO_WALL_SIGN;

@Mixin(net.minecraft.block.entity.BlockEntityType.class)
public abstract class BlockEntityTypeMixin {
    @ModifyArg(method = "<clinit>", at = @At(value = "INVOKE", target = "Lnet/minecraft/block/entity/BlockEntityType$Builder;create(Ljava/util/function/Supplier;[Lnet/minecraft/block/Block;)Lnet/minecraft/block/entity/BlockEntityType$Builder;", ordinal = 7), index = 1)
    private static Block[] modifyArgClinit(Block... blocks) {
        final Block[] RES = new Block[blocks.length + 2];
        for (int i = 0; i < blocks.length; i++) {
            RES[i] = blocks[i];
        }
        RES[blocks.length] = BAMBOO_SIGN;
        RES[blocks.length + 1] = BAMBOO_WALL_SIGN;
        return RES;
    }
}