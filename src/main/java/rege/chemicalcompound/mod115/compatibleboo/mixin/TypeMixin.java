package rege.chemicalcompound.mod115.compatibleboo.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.vehicle.BoatEntity.Type;

import static rege.chemicalcompound.mod115.compatibleboo.CompatiBlebooMain.BAMBOO;

@Mixin(net.minecraft.entity.vehicle.BoatEntity.Type.class)
public abstract class TypeMixin {
    @Inject(method = "getType(I)Lnet/minecraft/entity/vehicle/BoatEntity$Type;", at = @At("RETURN"), cancellable = true)
    private static void injectGetType(int i, CallbackInfoReturnable<Type> info) {
        if (i == BAMBOO.ordinal()) {
            info.setReturnValue(BAMBOO);
        }
    }

    @Inject(method = "getType(Ljava/lang/String;)Lnet/minecraft/entity/vehicle/BoatEntity$Type;", at = @At("RETURN"), cancellable = true)
    private static void injectGetType(String string, CallbackInfoReturnable<Type> info) {
        if (string.equals(BAMBOO.getName())) {
            info.setReturnValue(BAMBOO);
        }
    }
}
