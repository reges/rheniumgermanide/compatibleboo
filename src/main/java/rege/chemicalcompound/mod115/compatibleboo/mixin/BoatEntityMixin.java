package rege.chemicalcompound.mod115.compatibleboo.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.vehicle.BoatEntity;
import net.minecraft.item.Item;

import static rege.chemicalcompound.mod115.compatibleboo.CompatiBlebooMain.BAMBOO;
import static rege.chemicalcompound.mod115.compatibleboo.CompatiBlebooMain.BAMBOO_RAFT_ITEM;

@Mixin(net.minecraft.entity.vehicle.BoatEntity.class)
public abstract class BoatEntityMixin {
    @Shadow
    public abstract BoatEntity.Type getBoatType();

    @Inject(method = "asItem", at = @At("RETURN"), cancellable = true)
    private void injectAsItem(CallbackInfoReturnable<Item> info) {
        if (this.getBoatType() == BAMBOO) {
            info.setReturnValue(BAMBOO_RAFT_ITEM);
        }
    }
}
