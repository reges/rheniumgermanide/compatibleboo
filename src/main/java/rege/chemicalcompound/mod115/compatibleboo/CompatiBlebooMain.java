package rege.chemicalcompound.mod115.compatibleboo;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.minecraft.entity.vehicle.BoatEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SignItem;
import net.minecraft.item.TallBlockItem;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FenceBlock;
import net.minecraft.block.FenceGateBlock;
import net.minecraft.block.MapColor;
import net.minecraft.block.PillarBlock;
import net.minecraft.block.SignBlock;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.WallSignBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundEvent;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Direction;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import org.apache.logging.log4j.Logger;

import static net.minecraft.block.AbstractBlock.Settings;
import static net.minecraft.block.Material.DECORATION;
import static net.minecraft.block.Material.WOOD;
import static net.minecraft.item.ItemGroup.BUILDING_BLOCKS;
import static net.minecraft.item.ItemGroup.DECORATIONS;
import static org.apache.logging.log4j.LogManager.getLogger;

public class CompatiBlebooMain implements ModInitializer {
    // This logger is used to write text to the console and the log file.
    // It is considered best practice to use your mod id as the logger's name.
    // That way, it's clear which mod wrote info, warnings, and errors.
    /**
     * Class logger.
     */
    public static final Logger LOGGER = getLogger(CompatiBlebooMain.class);

    public static final BoatEntity.Type BAMBOO;

    public static final SoundEvent BLOCK_BAMBOO_WOOD_STEP = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood.step", new SoundEvent(new Identifier("block.bamboo_wood.step")));
    public static final SoundEvent BLOCK_BAMBOO_WOOD_PLACE = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood.place", new SoundEvent(new Identifier("block.bamboo_wood.place")));
    public static final SoundEvent BLOCK_BAMBOO_WOOD_HIT = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood.hit", new SoundEvent(new Identifier("block.bamboo_wood.hit")));
    public static final SoundEvent BLOCK_BAMBOO_WOOD_FALL = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood.fall", new SoundEvent(new Identifier("block.bamboo_wood.fall")));
    public static final SoundEvent BLOCK_BAMBOO_WOOD_BREAK = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood.break", new SoundEvent(new Identifier("block.bamboo_wood.break")));
    public static final SoundEvent BLOCK_BAMBOO_WOOD_DOOR_CLOSE = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood_door.close", new SoundEvent(new Identifier("block.bamboo_wood_door.close")));
    public static final SoundEvent BLOCK_BAMBOO_WOOD_DOOR_OPEN = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood_door.open", new SoundEvent(new Identifier("block.bamboo_wood_door.open")));
    public static final SoundEvent BLOCK_BAMBOO_WOOD_TRAPDOOR_CLOSE = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood_trapdoor.close", new SoundEvent(new Identifier("block.bamboo_wood_trapdoor.close")));
    public static final SoundEvent BLOCK_BAMBOO_WOOD_TRAPDOOR_OPEN = Registry.register(Registry.SOUND_EVENT, "block.bamboo_wood_trapdoor.open", new SoundEvent(new Identifier("block.bamboo_wood_trapdoor.open")));
    public static final BlockSoundGroup BAMBOO_WOOD = new BlockSoundGroup(1f, 1f, BLOCK_BAMBOO_WOOD_BREAK, BLOCK_BAMBOO_WOOD_STEP, BLOCK_BAMBOO_WOOD_PLACE, BLOCK_BAMBOO_WOOD_HIT, BLOCK_BAMBOO_WOOD_FALL);

    public static final Block BAMBOO_PLANKS = new Block(Settings.of(WOOD, MapColor.YELLOW).strength(2f, 3f).sounds(BAMBOO_WOOD));
    public static final Block BAMBOO_MOSAIC = new Block(Settings.of(WOOD, MapColor.YELLOW).strength(2f, 3f).sounds(BAMBOO_WOOD));

    public static class StairsBlock extends net.minecraft.block.StairsBlock {
        public StairsBlock(BlockState a1, Settings a2) {
            super(a1, a2);
        }
    }
    public static final StairsBlock BAMBOO_STAIRS = new StairsBlock(BAMBOO_PLANKS.getDefaultState(), Settings.copy(BAMBOO_PLANKS));
    public static final StairsBlock BAMBOO_MOSAIC_STAIRS = new StairsBlock(BAMBOO_MOSAIC.getDefaultState(), Settings.copy(BAMBOO_MOSAIC));

    public static final SlabBlock BAMBOO_SLAB = new SlabBlock(Settings.of(WOOD, MapColor.YELLOW).strength(2f, 3f).sounds(BAMBOO_WOOD));
    public static final SlabBlock BAMBOO_MOSAIC_SLAB = new SlabBlock(Settings.of(WOOD, MapColor.YELLOW).strength(2f, 3f).sounds(BAMBOO_WOOD));

    public static final PillarBlock BAMBOO_BLOCK = new PillarBlock(Settings.of(WOOD, blockState -> blockState.get(PillarBlock.AXIS) == Direction.Axis.Y ? MapColor.YELLOW : MapColor.DARK_GREEN).strength(2.0f).sounds(BAMBOO_WOOD));
    public static final PillarBlock STRIPPED_BAMBOO_BLOCK = new PillarBlock(Settings.of(WOOD, blockState -> MapColor.YELLOW).strength(2.0f).sounds(BAMBOO_WOOD));

    public static class SignType extends net.minecraft.util.SignType {
        public static final SignType BAMBOO = new SignType("bamboo");

        public SignType(String a1) {
            super(a1);
        }
    }

    public static final SignBlock BAMBOO_SIGN = new SignBlock(Settings.of(WOOD).noCollision().strength(1.0f).sounds(BAMBOO_WOOD), SignType.BAMBOO);
    public static final WallSignBlock BAMBOO_WALL_SIGN = new WallSignBlock(Settings.of(WOOD).noCollision().strength(1.0f).sounds(BAMBOO_WOOD).dropsLike(BAMBOO_SIGN), SignType.BAMBOO);

    public static class PressurePlateBlock extends net.minecraft.block.PressurePlateBlock {
        public PressurePlateBlock(ActivationRule a1, Settings a2) {
            super(a1, a2);
        }
    }
    public static final PressurePlateBlock BAMBOO_PRESSURE_PLATE = new PressurePlateBlock(PressurePlateBlock.ActivationRule.EVERYTHING, Settings.of(WOOD, BAMBOO_PLANKS.getDefaultMapColor()).noCollision().strength(0.5f).sounds(BAMBOO_WOOD));
    public static class WoodenButtonBlock extends net.minecraft.block.WoodenButtonBlock {
        public WoodenButtonBlock(Settings a1) {
            super(a1);
        }
    }
    public static final WoodenButtonBlock BAMBOO_BUTTON = new WoodenButtonBlock(Settings.of(DECORATION).noCollision().strength(0.5f).sounds(BAMBOO_WOOD));

    public static class DoorBlock extends net.minecraft.block.DoorBlock {
        public DoorBlock(Settings a1) {
            super(a1);
        }
    }
    public static final DoorBlock BAMBOO_DOOR = new DoorBlock(Settings.of(WOOD, BAMBOO_PLANKS.getDefaultMapColor()).strength(3f).sounds(BAMBOO_WOOD).nonOpaque());
    public static class TrapdoorBlock extends net.minecraft.block.TrapdoorBlock {
        public TrapdoorBlock(Settings a1) {
            super(a1);
        }
    }
    public static final TrapdoorBlock BAMBOO_TRAPDOOR = new TrapdoorBlock(Settings.of(WOOD, MapColor.YELLOW).strength(3f).sounds(BAMBOO_WOOD).nonOpaque().allowsSpawning((a1, a2, a3, a4) -> false));

    public static final FenceBlock BAMBOO_FENCE = new FenceBlock(Settings.of(WOOD, BAMBOO_PLANKS.getDefaultMapColor()).strength(2f, 3f).sounds(BAMBOO_WOOD));
    public static final FenceGateBlock BAMBOO_FENCE_GATE = new FenceGateBlock(Settings.of(WOOD, BAMBOO_PLANKS.getDefaultMapColor()).strength(2f, 3f).sounds(BAMBOO_WOOD));

    private static final Item.Settings ITEM_SETTINGS = new Item.Settings().maxCount(64).group(BUILDING_BLOCKS);
    public static final BlockItem BAMBOO_PLANKS_ITEM = new BlockItem(BAMBOO_PLANKS, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_MOSAIC_ITEM = new BlockItem(BAMBOO_MOSAIC, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_STAIRS_ITEM = new BlockItem(BAMBOO_STAIRS, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_MOSAIC_STAIRS_ITEM = new BlockItem(BAMBOO_MOSAIC_STAIRS, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_SLAB_ITEM = new BlockItem(BAMBOO_SLAB, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_MOSAIC_SLAB_ITEM = new BlockItem(BAMBOO_MOSAIC_SLAB, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_BLOCK_ITEM = new BlockItem(BAMBOO_BLOCK, ITEM_SETTINGS);
    public static final BlockItem STRIPPED_BAMBOO_BLOCK_ITEM = new BlockItem(STRIPPED_BAMBOO_BLOCK, ITEM_SETTINGS);
    public static final SignItem BAMBOO_SIGN_ITEM = new SignItem(new Item.Settings().maxCount(16).group(DECORATIONS), BAMBOO_SIGN, BAMBOO_WALL_SIGN);
    public static final BlockItem BAMBOO_PRESSURE_PLATE_ITEM = new BlockItem(BAMBOO_PRESSURE_PLATE, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_BUTTON_ITEM = new BlockItem(BAMBOO_BUTTON, ITEM_SETTINGS);
    public static final TallBlockItem BAMBOO_DOOR_ITEM = new TallBlockItem(BAMBOO_DOOR, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_TRAPDOOR_ITEM = new BlockItem(BAMBOO_TRAPDOOR, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_FENCE_ITEM = new BlockItem(BAMBOO_FENCE, ITEM_SETTINGS);
    public static final BlockItem BAMBOO_FENCE_GATE_ITEM = new BlockItem(BAMBOO_FENCE_GATE, ITEM_SETTINGS);
    public static final Item BAMBOO_RAFT_ITEM = new Item(new Item.Settings().maxCount(1).group(ItemGroup.TRANSPORTATION)) {
        @Override
        public void appendTooltip(ItemStack itemStack, World world, List<Text> tooltip, TooltipContext tooltipContext) {
            tooltip.add(new TranslatableText("item.minecraft.bamboo_raft.tooltip"));
        }
    };

    static {
        try {
            Field f = Class.forName("sun.misc.Unsafe").getDeclaredField("theUnsafe");
            f.setAccessible(true);
            Object unsafe = f.get(null);
            Method method1 = unsafe.getClass().getMethod("allocateInstance", Class.class);
            BAMBOO = (BoatEntity.Type)method1.invoke(unsafe, BoatEntity.Type.class);
            method1 = unsafe.getClass().getMethod("putInt", Object.class, long.class, int.class);
            Method method2 = unsafe.getClass().getMethod("objectFieldOffset", Field.class);
            method1.invoke(unsafe, BAMBOO, method2.invoke(unsafe, Enum.class.getDeclaredField("ordinal")), 1479895287);
            method1 = unsafe.getClass().getMethod("putObject", Object.class, long.class, Object.class);
            method1.invoke(unsafe, BAMBOO, method2.invoke(unsafe, Enum.class.getDeclaredField("name")), "BAMBOO");
            method1.invoke(unsafe, BAMBOO, method2.invoke(unsafe, BoatEntity.Type.class.getDeclaredField("name")), "bamboo");
            method1.invoke(unsafe, BAMBOO, method2.invoke(unsafe, BoatEntity.Type.class.getDeclaredField("baseBlock")), BAMBOO_PLANKS);
        } catch (NoSuchFieldException | IllegalAccessException | ClassNotFoundException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onInitialize() {
        // This code runs as soon as Minecraft is in a mod-load-ready state.
        // However, some things (like resources) may still be uninitialized.
        // Proceed with mild caution.

        LOGGER.info("Running " + CompatiBlebooMain.class);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_planks"), BAMBOO_PLANKS);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_mosaic"), BAMBOO_MOSAIC);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_slab"), BAMBOO_SLAB);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_mosaic_slab"), BAMBOO_MOSAIC_SLAB);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_stairs"), BAMBOO_STAIRS);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_mosaic_stairs"), BAMBOO_MOSAIC_STAIRS);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_pressure_plate"), BAMBOO_PRESSURE_PLATE);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_button"), BAMBOO_BUTTON);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_door"), BAMBOO_DOOR);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_trapdoor"), BAMBOO_TRAPDOOR);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_sign"), BAMBOO_SIGN);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_wall_sign"), BAMBOO_WALL_SIGN);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_fence"), BAMBOO_FENCE);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_fence_gate"), BAMBOO_FENCE_GATE);
        Registry.register(Registry.BLOCK, new Identifier("bamboo_block"), BAMBOO_BLOCK);
        Registry.register(Registry.BLOCK, new Identifier("stripped_bamboo_block"), STRIPPED_BAMBOO_BLOCK);
        Registry.register(Registry.ITEM, new Identifier("bamboo_planks"), BAMBOO_PLANKS_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_mosaic"), BAMBOO_MOSAIC_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_slab"), BAMBOO_SLAB_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_mosaic_slab"), BAMBOO_MOSAIC_SLAB_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_stairs"), BAMBOO_STAIRS_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_mosaic_stairs"), BAMBOO_MOSAIC_STAIRS_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_pressure_plate"), BAMBOO_PRESSURE_PLATE_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_button"), BAMBOO_BUTTON_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_door"), BAMBOO_DOOR_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_trapdoor"), BAMBOO_TRAPDOOR_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_sign"), BAMBOO_SIGN_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_fence"), BAMBOO_FENCE_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_fence_gate"), BAMBOO_FENCE_GATE_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_raft"), BAMBOO_RAFT_ITEM);
        Registry.register(Registry.ITEM, new Identifier("bamboo_block"), BAMBOO_BLOCK_ITEM);
        Registry.register(Registry.ITEM, new Identifier("stripped_bamboo_block"), STRIPPED_BAMBOO_BLOCK_ITEM);
        FuelRegistry.INSTANCE.add(BAMBOO_MOSAIC_ITEM, 300);
        FuelRegistry.INSTANCE.add(BAMBOO_MOSAIC_SLAB_ITEM, 150);
        FuelRegistry.INSTANCE.add(BAMBOO_MOSAIC_STAIRS_ITEM, 300);
    }
}
