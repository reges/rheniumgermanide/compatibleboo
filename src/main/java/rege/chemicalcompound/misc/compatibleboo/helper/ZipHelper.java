package rege.chemicalcompound.misc.compatibleboo.helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipInputStream;

import org.jetbrains.annotations.Contract;

/**
 * @author RheniumGermanide
 */
public abstract class ZipHelper {
    /**
     * @param url URL to download from.
     * @param filepath Downloaded file in which to save.
     * @param filename Saved downloaded file's name.
     */
    public static void download(String url, String filepath, String filename) {
        try {
            final URL URL_OBJ = new URL(url);
            final HttpURLConnection CONN = (HttpURLConnection)(URL_OBJ.openConnection());
            CONN.setConnectTimeout(300000);
            final InputStream ISTREAM = CONN.getInputStream();
            final byte[] BUF = new byte[1024];
            int len = 0;
            int fetched = 0;
            final ByteArrayOutputStream BOS = new ByteArrayOutputStream();
            while ((len = ISTREAM.read(BUF)) != -1) {
                BOS.write(BUF, 0, len);
                fetched += 1024;
                System.err.print(Integer.toString(fetched)+" fetched\r");
            }
            BOS.close();
            writefile(BOS.toByteArray(), filepath, filename);
            ISTREAM.close();
        } catch (IOException e) {
           e.printStackTrace();
        }
    }

    /**
     * @param input {@code ZipInputStream} to read.
     * @return A byte array of read data.
     */
    public static byte[] readFromZip(ZipInputStream input) {
        try {
            ByteArrayOutputStream BOUT = new ByteArrayOutputStream();
            final byte[] TMP = new byte[1024];
            int length = 0;
            while ((length = input.read(TMP, 0, 1024)) != -1) {
                BOUT.write(TMP, 0, length);
            }
            final byte[] BUF = BOUT.toByteArray();
            BOUT.close();
            return BUF;
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @param body The body of a binary file to write.
     * @param filepath
     * @param filename
     * @throws IOException
     */
    public static void writefile(byte[] body, String filepath, String filename) throws IOException {
        final File DIR = new File(filepath);
        if (!DIR.exists()) {
            DIR.mkdirs();
        }
        final File FILE = new File(filepath+"/"+filename);
        final FileOutputStream FOS = new FileOutputStream(FILE);
        FOS.write(body);
        FOS.close();
    }

    @Contract("-> fail")
    protected ZipHelper() {
        throw new UnsupportedOperationException();
    }
}