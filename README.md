# CompatiBleboo
## FAQ
#### Q: What happened to the bamboo woods' textures?
**A**: I do not own the vanilla bamboo woods' textures, but I made a tool for
you to generare a vanilla bamboo woods' textures' resource pack. You can
generate it with:
`java -cp this_mod.jar rege.chemicalcompound.mod115.compatibleboo.GenVanillaBambooTextures`